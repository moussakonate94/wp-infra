provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket         = "ey-terraform-fitec-bucket-state-demo"
    key            = "global/s3/prod/data/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "ey-terraform-fitec-dynamodb-lock-demo"
    encrypt        = true
  }
}
